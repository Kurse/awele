# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## 2020-05-13
### Added
- CHANGELOG.md
- First iteration of a JavaFX frontend.

### Changed
- Renamed some methods for consistency.
- Updated sequence diagrams.


### Removed


