# Awele

This is my version of the Awele game for the Object Oriented Programming course from the [Institut de Technologie de
Liege](http://webitlg.portail.itlg.be).

## About the program...

The architecture of the program is inspired by the MVC pattern with an extra communication layer between the View and
the Controller.

Basically it comes down to this: the game board is represented by the `Board` class which is a simple bean. All the game
actions are handled by the ```BoardController``` class, which holds all the logic (moving seeds, checking if the rules
are being respected etc.).

Any interaction between an user interface (TUI or GUI) and the gameConteroller is handled through the `MessageStream`
class, which is a minimalist and synchronous implementation of a broadcast/subscription messaging service.

Whenever the `BoardController` executes an action, it broadcasts it to the `AweleMessageStream` which in turn notifies every
subscribed `AweleMessageHandler` of the action. This is how the UI passes user events to the `BoardController` and how the
`BoardController` notifies the UI of any board changes.

## Playing

A game can be started by calling `Awele.getInstance().play()`. 

A `Player` can either be a human or a computer, by calling the builder methods in `Player` (`Player.human()` or 
`Player.computer()`). This means a game can be played human vs. computer or human vs. human or computer vs. computer.
Changing the type of player is as simple as changing the `Player` array in `Awele.java`.

Similarly, the architecture is designed to have easy user interface changes. To create a new UI, create a new class 
extending `UserInterface` and implement the abstract methods.

### _Happy playing !_

