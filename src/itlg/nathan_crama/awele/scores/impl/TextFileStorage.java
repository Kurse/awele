package itlg.nathan_crama.awele.scores.impl;

import itlg.nathan_crama.awele.scores.AweleStorage;
import itlg.nathan_crama.awele.scores.models.Score;
import itlg.nathan_crama.tools.FileUtils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class TextFileStorage extends AweleStorage {
    private final Path path;

    public TextFileStorage() {
        this("./data/scores.awele");
    }

    public TextFileStorage(String filePath) {
        super();
       this.path = FileUtils.getFilePath(filePath);
    }

    @Override
    public void save(Score score) throws IOException {
        appendToFile(score.toStorableString());
    }

    @Override
    public List<Score> fetch() {
        return readFile();
    }

    private void appendToFile(String stuff) throws IOException {
        List<String> score = new ArrayList<>();
        score.add(stuff);
        Files.write(path, score, StandardCharsets.UTF_8, StandardOpenOption.APPEND);
    }

    private List<Score> readFile() {
        ArrayList<Score> scores = new ArrayList<>();
        try (Scanner sc = new Scanner(new File(path.toString()))) {
            while (sc.hasNextLine()) {
                scores.add(Score.fromString(sc.nextLine()));
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        return scores;
    }
}
