package itlg.nathan_crama.awele.scores.impl;

import itlg.nathan_crama.awele.scores.AweleStorage;
import itlg.nathan_crama.awele.scores.models.Score;

import java.io.File;
import java.sql.*;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class SQLiteStorage extends AweleStorage {

    private final String dbName = "scores.sqlite";

    private final String tableName = "scores";
    private final String idField = "id";
    private final String playerNameField = "player_name";
    private final String score1Field = "score1";
    private final String score2Field = "score2";
    private final String startField = "start";
    private final String durationField = "duration";

    public SQLiteStorage() throws SQLException, ClassNotFoundException {

        Connection connection = this.connect();
        boolean dbExists = new File(dbName).exists();

        Class.forName("org.sqlite.JDBC");
        System.out.println("SQLite connection successful");

        if (!dbExists) {
            // Create the database and the table if the file doesn't exist.
            Statement statement = connection.createStatement();
            String sql = "CREATE TABLE " + tableName + " (" +
                    idField + "             INT     PRIMARY KEY," +
                    playerNameField + "    TEXT    NOT NULL, " +
                    score1Field + "         INT     NOT NULL, " +
                    score2Field + "         INT     NOT NULL, " +
                    startField + "          DATE    NOT NULL, " +
                    durationField + "       INT     NOT NULL);";
            statement.executeUpdate(sql);
            statement.close();
        }
        connection.close();
    }

    private Connection connect() throws SQLException {
        return DriverManager.getConnection("jdbc:sqlite:" + dbName);
    }


    /**
     * Saves the @{link Score} sent by {@link AweleStorage} into the SQLite database.
     * @param score to save
     */
    @Override
    public void save(Score score)  {
        String insert = "INSERT INTO " +
                String.format("%s(%s, %s, %s, %s, %s)",
                              tableName,
                              playerNameField,
                              score1Field,
                              score2Field,
                              startField,
                              durationField) +
                " VALUES(?,?,?,?,?);";

        try (Connection connection = this.connect();
             PreparedStatement preparedStatement = connection.prepareStatement(insert)) {

            preparedStatement.setString(1, score.getPlayerName());
            preparedStatement.setInt(2, score.getResult1());
            preparedStatement.setInt(3, score.getResult2());
            long millisStart = score.getStart().atZone(ZoneId.systemDefault()).toInstant().toEpochMilli();
            preparedStatement.setDate(4, new java.sql.Date(millisStart));
            preparedStatement.setInt(5, score.getDuration());
            preparedStatement.executeUpdate();
        } catch (SQLException throwable) {
            System.err.println(Arrays.toString(throwable.getStackTrace()));
        }

    }

    /**
     * Fetches all the saved scores from the SQLite database and returns them as a list
     * @return List of saved scores
     */
    @Override
    public List<Score> fetch()  {
        List<Score> scores = new ArrayList<>();

        String select = "SELECT * FROM " + tableName;
        try (Connection connection = this.connect();
             Statement stmt  = connection.createStatement();
             ResultSet rs  = stmt.executeQuery(select)){

            // loop through the result set
            while (rs.next()) {
                Score score = new Score(
                        rs.getString(playerNameField),
                        rs.getInt(score1Field),
                        rs.getInt(score2Field),
                        rs.getLong(startField),
                        rs.getInt(durationField)
                );
                scores.add(score);
            }

        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return scores;
    }
}
