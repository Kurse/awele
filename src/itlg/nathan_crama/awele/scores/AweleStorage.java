package itlg.nathan_crama.awele.scores;

import itlg.nathan_crama.awele.Awele;
import itlg.nathan_crama.awele.message_stream.messages.output.AweleGameOver;
import itlg.nathan_crama.awele.message_stream.messages.output.AweleOutputMessage;
import itlg.nathan_crama.awele.scores.models.Score;
import itlg.nathan_crama.tools.message_stream.MessageHandler;
import itlg.nathan_crama.tools.storage.PersistentStorage;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;

/**
 * Abstract model for classes responsible for saving game data.
 */
public abstract class AweleStorage implements PersistentStorage<Score>, MessageHandler<AweleOutputMessage> {

    private LocalDateTime start;

    protected AweleStorage() {
        Awele.outputMessageStream.subscribe(this);
    }

    /**
     * This class listens for game start/end events to calculate the game duration and to automatically save game data
     * at the end of the games.
     * @param message
     */
    @Override
    public void handle(AweleOutputMessage message) {
        if (message.getMessageType() == AweleOutputMessage.MessageType.GAME_OVER) {
            handleGameOver((AweleGameOver) message);
        }

        if (message.getMessageType() == AweleOutputMessage.MessageType.GAME_RESTARTED) {
            handleNewGame();
        }

    }

    /**
     * Method executed when the class recieves a "new game" message.
     * Automatically saves the game start time used to calculate game duration + for the database entry.
     */
    protected void handleNewGame() {
        this.start = LocalDateTime.now();
    }

    /**
     * Gets the player 1's name to save in the database.
     * @return
     */
    private String getPlayerName() {
        return Awele.getInstance().getPlayers()[0].getName();
    }

    /**
     * Method executed when the class recieves a "game over" message. Automatically builds a {@link Score} object and
     * triggers the (inherited) save function.
     */
    private void handleGameOver(AweleGameOver message) {
        int duration = (int) start.until(LocalDateTime.now(), ChronoUnit.SECONDS);
        int[] scores = message.getBoard().scoreHoles;
        String playerName = getPlayerName();
        Score score = new Score(playerName, scores[0], scores[1], start, duration);

        try {
            this.save(score);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
