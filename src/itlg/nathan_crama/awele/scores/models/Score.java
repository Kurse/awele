package itlg.nathan_crama.awele.scores.models;

import java.time.Instant;
import java.time.LocalDateTime;
import java.util.Base64;
import java.util.TimeZone;

/**
 * Class used to represent the game state at the end of a game.
 */
public class Score {
    /**
     * Player 1's score
     */
    private int result1;

    /**
     * Player 2's score
     */
    private int result2;

    /**
     * Player 1's name
     */
    private String playerName;

    /**
     * Game start time
     */
    private LocalDateTime start;

    /**
     * Game duration in seconds
     */
    private int duration;

    public Score() {

    }

    /**
     * Instantiates a new Score.
     *
     * @param playerName Player 1's name
     * @param result1    Player 1's score
     * @param result2    Player 2's score
     * @param start      Game start time
     * @param duration   Game duation in seconds
     */
    public Score(String playerName, int result1, int result2, LocalDateTime start, int duration) {
        this();
        this.setResult1(result1);
        this.setResult2(result2);
        this.setPlayerName(playerName);
        this.setStart(start);
        this.setDuration(duration);
    }

    /**
     * Instantiates a new Score.
     *
     * @param playerName Player 1's name
     * @param result1    Player 1's score
     * @param result2    Player 2's score
     * @param start      Game start time (unix timestamp in ms)
     * @param duration   Game duation in seconds
     */
    public Score(String playerName, int result1, int result2, long start, int duration) {
        this(
                playerName,
                result1,
                result2,
                LocalDateTime.ofInstant(Instant.ofEpochMilli(start), TimeZone.getDefault().toZoneId()),
                duration
        );
    }

    /**
     * Instantiates a Score from a string (format from {@link this#toStorableString()})
     * @param string the string
     * @return the score
     */
    public static Score fromString(String string) {
        String[] split = string.split("\t");

        if (split.length < 4) {
            return null;
        }

        Score score = new Score();
        score.setStart(split[0]);
        score.setDuration(split[1]);
        score.setResult1(split[2]);
        score.setResult2(split[3]);
        score.setBase64EncodedPlayerName(split[4]);
        return score;
    }

    /**
     * Gets game duration in seconds.
     *
     * @return the duration
     */
    public int getDuration() {
        return duration;
    }

    public void setBase64EncodedPlayerName(String s) {
        this.playerName = new String(Base64.getDecoder().decode(s));
    }

    /**
     * Gets result 1.
     *
     * @return the result 1
     */
    public int getResult1() {
        return result1;
    }

    /**
     * Gets result 2.
     *
     * @return the result 2
     */
    public int getResult2() {
        return result2;
    }

    /**
     * Gets player name.
     *
     * @return the player name
     */
    public String getPlayerName() {
        return playerName;
    }

    @Override
    public String toString() {
        return this.toString(this.playerName);
    }

    /**
     * To storable string string. Converts the name to base-64 to ensure support for special characters, other encodings
     * or other alphabets.
     *
     * @return the string
     */
    public String toStorableString() {
        String name = Base64.getEncoder().encodeToString(playerName.getBytes());
        return this.toString(name);
    }

    private String toString(String name) {
        return String.format("%s\t%d\t%d\t%d\t%s", start.toString(), duration, result1, result2, name);
    }

    public void setResult1(String result1) {
        this.result1 = Integer.parseInt(result1);
    }

    public void setResult2(String result2) {
        this.result2 = Integer.parseInt(result2);
    }

    private void setResult1(int result1) {
        this.result1 = result1;
    }

    private void setResult2(int result2) {
        this.result2 = result2;
    }

    private void setPlayerName(String playerName) {
        this.playerName = playerName;
    }

    /**
     * Sets start.
     *
     * @param start the start
     */
    private void setStart(LocalDateTime start) {
        this.start = start;
    }


    /**
     * Sets start.
     *
     * @param start the start
     */
    private void setStart(String start) {
        this.start = LocalDateTime.parse(start);
    }

    /**
     * Sets duration.
     *
     * @param duration the duration
     */
    private void setDuration(int duration) {
        this.duration = duration;
    }

    /**
     * Sets duration.
     *
     * @param duration the duration
     */
    private void setDuration(String duration) {
        this.duration = Integer.parseInt(duration);
    }

    /**
     * Gets game start time.
     *
     * @return the start
     */
    public LocalDateTime getStart() {
        return start;
    }
}
