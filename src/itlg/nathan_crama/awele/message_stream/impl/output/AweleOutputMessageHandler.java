package itlg.nathan_crama.awele.message_stream.impl.output;

import itlg.nathan_crama.awele.message_stream.messages.output.*;
import itlg.nathan_crama.awele.models.Board;
import itlg.nathan_crama.awele.models.Index;
import itlg.nathan_crama.tools.message_stream.Message;
import itlg.nathan_crama.tools.message_stream.MessageHandler;

/**
 * Awele-specific implementation of {@link EventHandler}.
 * Creates callback methods for every {@link AweleEvent.EventType}
 *
 * @see MessageHandler
 * @see Message
 * @see AweleOutputMessage
 * @see AweleOutputMessage.MessageType
 */
public abstract class AweleOutputMessageHandler implements MessageHandler<AweleOutputMessage> {

    @Override
    public void handle(AweleOutputMessage message) {
        switch (message.getMessageType()) {
            case BOARD_CHANGE:
                Board board = ((AweleBoardChange) message).getBoard();
                this.handleBoardChange(board);
                break;
            case GAME_OVER:
                System.out.println("Game over");
                Board score = ((AweleGameOver) message).getBoard();
                this.handleGameOver(score);
                break;
            case INVALID_MOVE:
                this.handleInvalidMove((AweleError) message);
                break;
            case READY_FOR_NEW_MOVE:
                AweleReadyForNewMove nm = (AweleReadyForNewMove) message;
                this.handleReadyForNewMove(nm.isHuman(), nm.getIndex());
                break;
            case MOVE_IS_VALID:
                AweleMoveIsValid validMove = (AweleMoveIsValid) message;
                this.handleValidMove(validMove.getIndex());
                break;
            case DID_SCORE:
                AwelePlayerScored playerScored = (AwelePlayerScored) message;
                this.handlePlayerScored(playerScored);
                break;
            case GAME_RESTARTED:
                this.handleRestart();
                break;
            default:
                System.out.println("Event ignored: " + message.getMessageType());
                break;
        }
    }

    protected abstract void handlePlayerScored(AwelePlayerScored playerScored);

    protected abstract void handleGameOver(Board board);

    protected abstract void handleBoardChange(Board board);

    protected abstract void handleInvalidMove(AweleError event);

    protected abstract void handleReadyForNewMove(boolean isHuman, int row);

    protected abstract void handleValidMove(Index index);

    protected abstract void handleRestart();
}
