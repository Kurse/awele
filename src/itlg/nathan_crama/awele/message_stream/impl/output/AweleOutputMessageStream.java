package itlg.nathan_crama.awele.message_stream.impl.output;

import itlg.nathan_crama.awele.message_stream.messages.output.AweleError;
import itlg.nathan_crama.awele.message_stream.messages.output.AweleOutputMessage;
import itlg.nathan_crama.awele.models.Index;
import itlg.nathan_crama.tools.Logger;
import itlg.nathan_crama.tools.message_stream.MessageHandler;
import itlg.nathan_crama.tools.message_stream.MessageStream;

public class AweleOutputMessageStream extends MessageStream<AweleOutputMessage> {

    @Override
    public void publish(AweleOutputMessage event) {
        if (event.getMessageType() == AweleOutputMessage.MessageType.INVALID_MOVE) {
            AweleError error = (AweleError) event;
            AweleError.ErrorType errorType = error.getErrorType();
            Index index = error.getIndex();
            Logger.error(errorType + " at index " + index.getRow() + " " + index.getHole());

        } else {
            Logger.info("Event received: " + event.getMessageType());
        }
        super.publish(event);
    }

    @Override
    public void subscribe(MessageHandler<AweleOutputMessage> handler) {
        Logger.info("New subscriber: " + handler.getClass());
        super.subscribe(handler);
    }
}
