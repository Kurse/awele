package itlg.nathan_crama.awele.message_stream.impl.input;

import itlg.nathan_crama.awele.message_stream.messages.input.AweleInputMessage;
import itlg.nathan_crama.awele.message_stream.messages.input.AweleMove;
import itlg.nathan_crama.awele.models.Index;
import itlg.nathan_crama.tools.Logger;
import itlg.nathan_crama.tools.message_stream.MessageHandler;

public abstract class AweleInputMessageHandler implements MessageHandler<AweleInputMessage> {

    @Override
    public void handle(AweleInputMessage message) {
        switch (message.getMessageType()) {
            case NEW_MOVE_GIVEN:
                AweleMove move = (AweleMove) message;
                this.handleMove(move.getIndex());
                break;
            case RESTART_GAME:
                this.handleRestart();
                break;
            default:
                Logger.info("Event ignored: " + message.getMessageType());
                break;
        }
    }

    protected abstract void handleMove(Index index);

    protected abstract void handleRestart();
}
