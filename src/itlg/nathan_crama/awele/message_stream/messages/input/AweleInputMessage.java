package itlg.nathan_crama.awele.message_stream.messages.input;

import itlg.nathan_crama.awele.models.Index;
import itlg.nathan_crama.tools.message_stream.Message;

public abstract class AweleInputMessage implements Message {

    private final MessageType messageType;

    public enum MessageType {
        NEW_MOVE_GIVEN,
        RESTART_GAME,
    }

    AweleInputMessage(MessageType type) {
        this.messageType = type;
    }


    public static AweleInputMessage requestRestart() {
        return new AweleInputMessage(MessageType.RESTART_GAME) {
            @Override
            public MessageType getMessageType() {
                return super.getMessageType();
            }
        };
    }

    public static AweleInputMessage newMoveGiven(Index index) {
        return new AweleMove(index);
    }

    public MessageType getMessageType() {
        return messageType;
    }
}
