package itlg.nathan_crama.awele.message_stream.messages.input;

import itlg.nathan_crama.awele.models.Index;

/**
 * Event representing a player action.
 */
public class AweleMove extends AweleInputMessage {
    private final Index index;

    AweleMove(Index index) {
        super(MessageType.NEW_MOVE_GIVEN);
        this.index = index;
    }

    public Index getIndex() {
        return this.index;
    }
}
