package itlg.nathan_crama.awele.message_stream.messages.output;

import itlg.nathan_crama.awele.models.Board;
import itlg.nathan_crama.awele.models.Index;
import itlg.nathan_crama.tools.message_stream.Message;

/**
 * Awele-specific implementation of an {@link Message}.
 * Different types of events are listed in the enum {@link MessageType}.
 *
 * @see Message
 */
public abstract class AweleOutputMessage implements Message {
    public enum MessageType {
        BOARD_CHANGE,
        GAME_OVER,
        INVALID_MOVE,
        READY_FOR_NEW_MOVE,
        MOVE_IS_VALID,
        DID_SCORE,
        GAME_RESTARTED,
    }

    private final MessageType messageType;

    protected AweleOutputMessage(MessageType type) {
        this.messageType = type;
    }

    public MessageType getMessageType() {
        return messageType;
    }

    /*
     * Shorthand methods to create every kind of message.
     */
    public static AweleOutputMessage error(Index index, AweleError.ErrorType errorType) {
        return new AweleError(index, errorType);
    }

    public static AweleOutputMessage rowError(Index index, int actualRow) {
        return AweleError.rowError(index, actualRow);
    }

    public static AweleOutputMessage gameOver(Board board) {
        return new AweleGameOver(board);
    }

    public static AweleBoardChange boardChange(Board board) {
        return new AweleBoardChange(board);
    }

    public static AweleOutputMessage readyForNewMove(boolean isHuman, int row) {
        return new AweleReadyForNewMove(isHuman, row);
    }

    public static AweleOutputMessage moveIsValid(Index index) {
        return new AweleMoveIsValid(index);
    }

    public static AweleOutputMessage playerScored(int score, int player) {
        return new AwelePlayerScored(score, player);
    }

    public static AweleOutputMessage gameRestarted() {
        return new AweleOutputMessage(MessageType.GAME_RESTARTED) {
            @Override
            public MessageType getMessageType() {
                return super.getMessageType();
            }
        };
    }
}
