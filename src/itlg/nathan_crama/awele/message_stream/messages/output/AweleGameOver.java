package itlg.nathan_crama.awele.message_stream.messages.output;

import itlg.nathan_crama.awele.models.Board;

/**
 * Event representing the end of a game. The board is passed on to the constructor to be able to
 * determine which player is the winner.
 */
public class AweleGameOver extends AweleOutputMessage {
    private final Board board;

    AweleGameOver(Board board) {
        super(MessageType.GAME_OVER);
        this.board = (Board) board.clone();
    }

    public Board getBoard() {
        return board;
    }
}
