package itlg.nathan_crama.awele.message_stream.messages.output;

import itlg.nathan_crama.awele.models.Board;
import itlg.nathan_crama.tools.message_stream.Message;

/**
 * Event representing a new state for the Board
 *
 * @see Board
 * @see Message
 * @see AweleOutputMessage
 */
public class AweleBoardChange extends AweleOutputMessage {
    private final Board board;

    AweleBoardChange(Board board) {
        super(MessageType.BOARD_CHANGE);
        // clone the board to avoid changing the main board by mistake
        this.board = (Board) board.clone();
    }

    public Board getBoard() {
        return board;
    }
}
