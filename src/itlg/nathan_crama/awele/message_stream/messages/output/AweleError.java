package itlg.nathan_crama.awele.message_stream.messages.output;

import itlg.nathan_crama.awele.Awele;
import itlg.nathan_crama.awele.models.Index;
import itlg.nathan_crama.tools.message_stream.Message;

/**
 * Event representing an error that has occured in the game.
 * Different types of errors are listed in the enum {@link AweleError.ErrorType}.
 *
 * @see Message
 * @see AweleOutputMessage
 */
public class AweleError extends AweleOutputMessage {
    private final int playerIndex;
    private final boolean human;
    private final Index index;
    private final ErrorType errorType;

    public enum ErrorType {
        OUT_OF_BOUNDS,
        EMPTY_HOLE,
        MOVE_WILL_STARVE, WRONG_ROW,
    }

    AweleError(Index index, ErrorType errorType) {
        super(MessageType.INVALID_MOVE);
        this.index = index;
        this.errorType = errorType;
        this.human = getIsHuman(index);
        this.playerIndex = index.getRow();
    }

    private AweleError(Index index, ErrorType errorType, boolean isHuman, int playerIndex) {
        super(MessageType.INVALID_MOVE);
        this.index = index;
        this.errorType = errorType;
        this.human = isHuman;
        this.playerIndex = playerIndex;
    }

    public static AweleError rowError(Index index, int row) {
        return new AweleError(index, ErrorType.WRONG_ROW, getIsHuman(row), row);
    }

    private static boolean getIsHuman(Index index) {
        return getIsHuman(index.getRow());
    }

    private static boolean getIsHuman(int index) {
        return Awele.getInstance().getPlayers()[index].isHuman();
    }

    public int getPlayerIndex() {
        return playerIndex;
    }

    public boolean isHuman() {
        return human;
    }

    public Index getIndex() {
        return index;
    }

    public ErrorType getErrorType() {
        return errorType;
    }
}
