package itlg.nathan_crama.awele.message_stream.messages.output;

public class AwelePlayerScored extends AweleOutputMessage {

    private final int playerIndex;
    private final int score;

    AwelePlayerScored(int points, int player) {
        super(MessageType.DID_SCORE);
        this.playerIndex = player;
        this.score = points;
    }

    public int getPlayerIndex() {
        return playerIndex;
    }

    public int getScore() {
        return score;
    }
}
