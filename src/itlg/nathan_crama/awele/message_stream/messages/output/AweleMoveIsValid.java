package itlg.nathan_crama.awele.message_stream.messages.output;

import itlg.nathan_crama.awele.models.Index;

/**
 * Event representing a confirmation that a given move is indeed valid.
 */
public class AweleMoveIsValid extends AweleOutputMessage {
    private final Index index;

    AweleMoveIsValid(Index index) {
        super(MessageType.MOVE_IS_VALID);
        this.index = index;
    }

    public Index getIndex() {
        return index;
    }

}
