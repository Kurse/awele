package itlg.nathan_crama.awele.message_stream.messages.output;

/**
 * Event representing the fact that the game is now idle and waiting for a new move to be given.
 */
public class AweleReadyForNewMove extends AweleOutputMessage {
    private final boolean isHuman;
    private final int index;

    AweleReadyForNewMove(boolean isHuman, int index) {
        super(MessageType.READY_FOR_NEW_MOVE);
        this.isHuman = isHuman;
        this.index = index;

    }

    public boolean isHuman() {
        return isHuman;
    }

    public int getIndex() {
        return index;
    }
}
