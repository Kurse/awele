package itlg.nathan_crama.awele.controllers;

import itlg.nathan_crama.awele.Awele;
import itlg.nathan_crama.awele.message_stream.impl.input.AweleInputMessageHandler;
import itlg.nathan_crama.awele.message_stream.impl.output.AweleOutputMessageStream;
import itlg.nathan_crama.awele.message_stream.messages.output.AweleError;
import itlg.nathan_crama.awele.message_stream.messages.output.AweleOutputMessage;
import itlg.nathan_crama.awele.models.Board;
import itlg.nathan_crama.awele.models.Index;
import itlg.nathan_crama.awele.models.Player;
import itlg.nathan_crama.tools.Logger;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * Class responsible for all actions regarding the Board and handling the game controllers.
 * Every action is executed here and "broadcast" through a publish/subscribe messaging service inspired by
 * JavaEE's JMS (Java Messaging Service).
 *
 * @see AweleOutputMessageStream
 */
public class BoardController extends AweleInputMessageHandler {

    private final Board board;
    private int turn = 0;
    private final Player[] players;

    public BoardController(Board board, Player[] players) {
        this.board = board;
        this.players = players;
        if (!board.isCloned()) {
            Awele.inputMessageStream.subscribe(this);
        }
    }

    public Player[] getPlayers() {
        return this.players;
    }

    /**
     * Method responsible for the turn-based system. Every time a move is played the turn variable gets incremented
     * and we use a modulus to determine whose player turn it is.
     *
     * @return 0 for player 1, 1 for player 2
     * @see itlg.nathan_crama.awele.controllers.BoardController#turn
     */
    private int getTurn() {
        return turn % 2;
    }


    /**
     * Method that executes a move given by a player.
     * It has a boolean flag to check if the move is a simulation or not (used to check if a move is valid).
     * If test is set to true,
     *
     * @param index Move to execute
     */
    private void executeMove(Index index) {
        if (!this.isMoveValid(index)) {
            return;
        }
        this.onValidMove(index);

        List<Index> sownHoles = sow(index);
        int points = reap(sownHoles);
        allocatePoints(points);
        this.turn += 1;


        if (isGameOver()) {
            this.onGameOver();
        } else {
            this.onBoardChange();
            this.readyForNewMove();
        }
    }

    private void allocatePoints(int points) {
        int playerRow = getTurn();
        if (points > 0) {
            this.onPlayerScored(points, playerRow);
        }

        int initialPoints = board.scoreHoles[playerRow];
        board.scoreHoles[playerRow] = initialPoints + points;
    }

    /**
     * Checks if the game is over.
     * The game is considered over if :
     * - 1 of the players has more than half the available points
     * - 1 of the players has no moves left
     *
     * @return {@link true} if the itlg.nathan_crama.awele is over.
     */
    private boolean isGameOver() {
        if (board.scoreHoles[0] > Board.HOLES_PER_ROW * Board.SEEDS_PER_HOLE
                || board.scoreHoles[1] > Board.HOLES_PER_ROW * Board.SEEDS_PER_HOLE) {
            // 1 player has more than half the available points. The other player can't win at this point
            return true;
        }

        if (this.isRowEmpty(getTurn())) {
            return true;
        }

        final int totalPoints = Board.HOLES_PER_ROW * Board.ROWS * Board.SEEDS_PER_HOLE;
        if (board.scoreHoles[0] + board.scoreHoles[1] == totalPoints - 2) {
            /* Only 2 seeds left: Maybe we have a tourniquet going on.
            e.g.:

            | 23 ||
            |  1 ||  0 ||  0 ||  0 ||  0 ||  0 |
          ->|  0 ||  0 ||  0 ||  0 ||  0 ||  1 |
            | 23 ||

            PS: This situation also sucks.
            | 21 ||
            |  0 ||  0 ||  0 ||  0 ||  0 ||  1 |
          ->|  0 ||  0 ||  4 ||  0 ||  0 ||  1 |
            | 21 ||

            * This game will go on forever.
            * */

            // This is bad but it fixes things:
            if (getHoleAtIndex(new Index(0, 0)) == 1 && getHoleAtIndex(new Index(1, 0)) == 1) {
                Logger.info("Wheelie confirmed");
                return true;
            }

            Logger.info("Wheelie suspected");
            for (int i = 0; i < Board.ROWS; i++) {
                for (int j = 0; j < Board.HOLES_PER_ROW; j++) {
                    if (1 == board.holes[i][j]) {
                        Index index = new Index(i, j);
                        for (int k = 0; k < Board.HOLES_PER_ROW - getTurn(); k++) {
                            index = getNextHole(index);
                        }

                        if (getHoleAtIndex(index) == 1) {
                            Logger.info("Wheelie confirmed");
                            return true;
                        }
                    }
                }
            }
        }

        List<Index> validMoves = getValidMoves();
        if (validMoves.isEmpty()) {
            return true;
        }

        boolean gameOver = true;
        int row = getTurn();
        for (int i = 0; i < Board.HOLES_PER_ROW; i++) {
            // Check every possible move 1 by 1
            Index index = new Index(row, i);
            gameOver = gameOver && willMoveStarveAdversary(index);
        }

        return gameOver;
    }

    private List<Index> getValidMoves() {
        ArrayList<Index> validMoves = new ArrayList<>();
        Index index;
        int row = getTurn();
        for (int i = 0; i < Board.HOLES_PER_ROW; i++) {
            index = new Index(row, i);
            if (isMoveValid(index, false)) {
                validMoves.add(index);
            }
        }
        return validMoves;
    }

    /**
     * Checks if the given move will leave the adversary with no moves left.
     * This method creates a clone of the current main board to simulate the plays without affecting the main
     * Board.
     *
     * @param index the move to check.
     * @return true if the move will starve the opponent.
     * @see Board
     */
    private boolean willMoveStarveAdversary(Index index) {
        Board clonedBoard = (Board) Board.mainBoard.clone();
        BoardController bc = new BoardController(clonedBoard, players);

        bc.reap(bc.sow(index));
        int adversaryRow = (index.getRow() + 1) % 2;
        return bc.isRowEmpty(adversaryRow);
    }

    /**
     * Method representing the act of "sowing" in Awele: taking the seeds from a hole and sowing them 1 by 1
     * into the following holes.
     *
     * @param start Index of hte hole to sow
     * @return List of the holes that have been sown.
     */
    private List<Index> sow(Index start) {
        Index index = start;

        List<Index> sownHolesCoordinates = new ArrayList<>();
        int seeds = this.emptyHoleAtIndex(index);
        for (int i = 0; i < seeds; i += 1) {
            Index nextHoleIndex = getNextHole(index);
            sownHolesCoordinates.add(nextHoleIndex);
            if (nextHoleIndex.equals(index)) {
                nextHoleIndex = getNextHole(index);
            }

            this.incrementHoleAtIndex(nextHoleIndex);
            index = nextHoleIndex;
        }
        return sownHolesCoordinates;
    }

    /**
     * Method representing the act of "reaping" in Awele: harvesting every subsequent hole, starting from the
     * last that contain 2 or 3 seeds and adding them to the captured seeds hole.
     *
     * @param holes List of holes to reap from.
     */
    private int reap(List<Index> holes) {
        int points = 0;
        for (int i = holes.size() - 1; i >= 0; i--) {
            Index coordinates = holes.get(i);
            int seeds = this.board.holes[coordinates.getRow()][coordinates.getHole()];
            if (seeds == 3 || seeds == 2) {
                points += seeds;
                this.board.holes[coordinates.getRow()][coordinates.getHole()] = 0;
            } else {
                break;
            }
        }

        return points;
    }

    /**
     * Sets up the board for a new game by emptying the score holes and filling the other holes with an
     * equal number of seeds each.
     */
    public void setupBoard() {
        // Sets the board up for a new game
        for (int i = 0; i < Board.ROWS; i++) {
            for (int j = 0; j < Board.HOLES_PER_ROW; j++) {
                this.board.holes[i][j] = Board.SEEDS_PER_HOLE;
            }
        }

        board.scoreHoles[0] = 0;
        board.scoreHoles[1] = 0;
        this.turn = 0;

        this.onBoardChange();
        this.readyForNewMove();
    }

    /**
     * Method used to get the next logical hole from the board. E.g. if the hole is the last of a row, return
     * the first of the next row.
     *
     * @param i Index of the hole to calculate the next hole from
     * @return Index of the next hole
     */
    private Index getNextHole(Index i) {
        Index index = (Index) i.clone();
        int h = index.getHole();
        if (h == Board.HOLES_PER_ROW - 1) {
            index.setHole(0);
            if (index.getRow() == Board.ROWS - 1) {
                index.setRow(0);
            } else {
                index.setRow(index.getRow() + 1);
            }
        } else {
            index.setHole(h + 1);
        }
        return index;
    }

    /**
     * Retrieves seeds from a hole given an Index
     *
     * @param i Index to get the seeds from;
     * @return seeds at the given index
     */
    private int getHoleAtIndex(Index i) {
        return board.holes[i.getRow()][i.getHole()];
    }

    /**
     * Sets the given number of seeds at the given index
     *
     * @param i index at which to set the seeds
     * @param x number of seeds to set
     */
    private void setHoleAtIndex(Index i, int x) {
        board.holes[i.getRow()][i.getHole()] = x;
    }

    /**
     * Increment the seeds at the given index by 1
     *
     * @param i Index to increment the seeds at
     */
    private void incrementHoleAtIndex(Index i) {
        incrementHoleAtIndex(i, 1);
    }

    /**
     * Increment the seeds at the given index by a given value
     *
     * @param i Index to increment the seeds at
     * @param x Number of seeds to add
     */
    private void incrementHoleAtIndex(Index i, int x) {
        board.holes[i.getRow()][i.getHole()] += x;
    }

    /**
     * Represents the physical action of taking all the seeds from a hole. Empties the hole and returns the value
     * that was stored in it.
     *
     * @param index Index to take the seeds at.
     * @return the number of seeds that were in the hole
     */
    private int emptyHoleAtIndex(Index index) {
        int i = getHoleAtIndex(index);
        setHoleAtIndex(index, 0);
        return i;
    }

    /**
     * Checks if the row at the given row is empty or not (as in contains no seeds).
     *
     * @param row Index of the row to check. Either 0 or 1
     * @return true if the row contains no seeds
     */
    private boolean isRowEmpty(int row) {
        for (int i = 0; i < board.holes[row].length; i++) {
            if (board.holes[row][i] > 0) {
                return false;
            }
        }

        return true;
    }

    /**
     * Method responsible for checking if a move is compliant with the rules.
     *
     * @param index Move to check
     * @return true if the move respects the rules
     */
    private boolean isMoveValid(Index index) {
        return isMoveValid(index, true);
    }

    /**
     * Method responsible for checking if a move is compliant with the rules.
     *
     * @param index       Move to check
     * @param shouldThrow True if you want to throw the errors, false if you want to silence them
     * @return true if the move respects the rules
     */
    private boolean isMoveValid(Index index, boolean shouldThrow) {
        int playerIndex = index.getRow();

        if (playerIndex != getTurn()) {
            if (shouldThrow) Awele.outputMessageStream.publish(AweleOutputMessage.rowError(index, getTurn()));
            return false;
        }

        if (index.getRow() < 0 || index.getRow() > Board.ROWS - 1 ||
                index.getHole() < 0 || index.getHole() > Board.HOLES_PER_ROW - 1) {
            if (shouldThrow) this.onError(index, AweleError.ErrorType.OUT_OF_BOUNDS);
            return false;
        }

        if (getHoleAtIndex(index) < 1) {
            if (shouldThrow) this.onError(index, AweleError.ErrorType.EMPTY_HOLE);
            return false;
        }

        if (willMoveStarveAdversary(index)) {
            if (shouldThrow) this.onError(index, AweleError.ErrorType.MOVE_WILL_STARVE);
            return false;
        }

        return true;
    }

    private void readyForNewMove() {
        boolean isHuman = this.players[getTurn()].isHuman();
        Awele.outputMessageStream.publish(AweleOutputMessage.readyForNewMove(isHuman, getTurn()));
    }

    private void onError(Index index, AweleError.ErrorType errorType) {
        Awele.outputMessageStream.publish(AweleOutputMessage.error(index, errorType));
    }

    private void onPlayerScored(int points, int playerRow) {
        Awele.outputMessageStream.publish(AweleOutputMessage.playerScored(points, playerRow));
    }

    private void onGameOver() {
        Awele.outputMessageStream.publish(AweleOutputMessage.gameOver(board));
    }

    private void onBoardChange() {
        Awele.outputMessageStream.publish(AweleOutputMessage.boardChange(this.board));
    }

    private void onValidMove(Index index) {
        Awele.outputMessageStream.publish(AweleOutputMessage.moveIsValid(index));
    }

    @Override
    protected void handleMove(Index index) {
        this.executeMove(index);
    }

    @Override
    protected void handleRestart() {
        this.setupBoard();
    }
}
