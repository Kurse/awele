package itlg.nathan_crama.awele.models;

public class ComputerPlayer extends Player {
    public ComputerPlayer() {
        super(false, "Machine");
    }
}
