package itlg.nathan_crama.awele.models;

import java.util.Random;

/**
 * This class represents an Awele player, wether it is human or not.
 * This is a simple bean.
 */
public class Player {
    /**
     * Flag used to determine if we should expect input from the player or not.
     */
    private final boolean isHuman;
    private String name;

    protected Player(boolean isHuman, String name) {
        this.isHuman = isHuman;
        setName(name);
    }

    public boolean isHuman() {
        return isHuman;
    }

    /**
     * Method used to generate an Index in case the Player is not human.
     *
     * @param playerIndex The row or player index (0 for player 1, 1 for player 2) to know which row to start from.
     * @return Index corresponding to the hole generated.
     */
    public static Index generateMove(int playerIndex) {
        int i = new Random().nextInt(Board.HOLES_PER_ROW);
        return new Index(playerIndex, i);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
