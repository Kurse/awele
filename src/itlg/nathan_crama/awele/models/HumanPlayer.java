package itlg.nathan_crama.awele.models;

public class HumanPlayer extends Player {
    public HumanPlayer(String name) {
        super(true, name);
    }
}
