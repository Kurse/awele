package itlg.nathan_crama.awele.models;

import itlg.nathan_crama.tools.Cloneable;
import java.util.Objects;

/**
 * This class represents the coordinates of a hole on the board. It is a simple bean.
 */
public class Index extends Cloneable {
    /**
     * Represents a row of the board:
     * 0 for player 1's row
     * 1 for player 2's board
     */
    private int row;

    /**
     * Represents a hole from a row:
     * 0 for the first hole
     * ...
     * 6 for the 5th hole
     */
    private int hole;

    public Index(int row, int holeIndex) {
        this.row = row;
        this.hole = holeIndex;
    }

    public int getRow() {
        return row;
    }

    public int getHole() {
        return hole;
    }

    public void setRow(int row) {
        this.row = row;
    }

    public void setHole(int hole) {
        this.hole = hole;
    }

    @Override
    public String toString() {
        return "Index{" +
                "row=" + row +
                ", holeIndex=" + hole +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Index index = (Index) o;
        return row == index.row &&
                hole == index.hole;
    }

    @Override
    public int hashCode() {
        return Objects.hash(row, hole);
    }
}
