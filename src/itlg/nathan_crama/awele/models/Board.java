package itlg.nathan_crama.awele.models;

import itlg.nathan_crama.tools.Cloneable;

/**
 * Board is meant to be a simple data class representing a physical game board.
 */
public class Board extends Cloneable {

    public final static int ROWS = 2;
    public final static int HOLES_PER_ROW = 6;
    public final static int SEEDS_PER_HOLE = 4;

    /**
     * mainBoard is not meant to be a singleton here:
     * It is used to get a common reference to a Board instance that every class can use.
     */
    public static final Board mainBoard = new Board();

    public final int[][] holes;
    public final int[] scoreHoles;

    private Board() {
        this.scoreHoles = new int[ROWS];
        this.holes = new int[ROWS][HOLES_PER_ROW];
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder()
          .append("| ")
          .append(String.format("%2d", scoreHoles[1]))
          .append(" ||\n");

        // Rangee joueur adverse
        for (int i=HOLES_PER_ROW-1; i >= 0; i--) {
            sb.append("| ")
              .append(String.format("%2d", holes[1][i]))
              .append(" |");
        }
        sb.append("\n");

        for (int j = 0; j < HOLES_PER_ROW; j++) {
            sb.append("| ")
              .append(String.format("%2d", holes[0][j]))
              .append(" |");

        }

        sb.append("\n")
          .append("| ")
          .append(String.format("%2d", scoreHoles[0]))
          .append(" ||");
        return sb.toString();
    }
}