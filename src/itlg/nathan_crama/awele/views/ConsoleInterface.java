package itlg.nathan_crama.awele.views;

import itlg.nathan_crama.awele.Awele;
import itlg.nathan_crama.awele.message_stream.impl.output.AweleOutputMessageHandler;
import itlg.nathan_crama.awele.message_stream.messages.input.AweleInputMessage;
import itlg.nathan_crama.awele.message_stream.messages.output.*;
import itlg.nathan_crama.awele.models.Board;
import itlg.nathan_crama.awele.models.Index;
import itlg.nathan_crama.awele.models.Player;
import itlg.nathan_crama.tools.Logger;

import java.util.Scanner;

public class ConsoleInterface extends UserInterface {

    /**
     * Method inherited from UserInterface. It is this UI implementation's version of a board view, so in this case
     * (a TUI or console interface), a simple print will do.
     *
     * @param board The board to display
     */
    @Override
    public void display(Board board) {
        Logger.info(board, false);
        System.out.println(board);
    }

    /**
     * Method called when the BoardController is done executing a player's move, and i signalling that it is ready
     * for a new move to be played.
     *
     * @param row The row or player index (0 for player 1, 1 for player 2) to know which row to start from.
     * @see ConsoleInterface#getPlayerAction(int)
     * @see itlg.nathan_crama.awele.models.ComputerPlayer#generateMove(int)
     */
    @Override
    public void handleReadyForNewMove(int row) {
        Index move = this.getPlayerAction(row);
        Awele.inputMessageStream.publish(AweleInputMessage.newMoveGiven(move));
    }

    /**
     * Method called to get an Index from a player (human or not). if the player is human, we prompt him for an action,
     * otherwise we generate an action.
     *
     * @param playerRow The row or player index (0 for player 1, 1 for player 2) to know which row to start from.
     * @return Index corresponding to the move selected or generated
     * @see Scanner
     * @see itlg.nathan_crama.awele.models.ComputerPlayer#generateMove(int)
     * @see Player
     * @see Player#isHuman()
     * @see Index
     */
    private Index getPlayerAction(int playerRow) {
        return new Index(playerRow, getPlayerAction(UIMessages.PICK_A_HOLE));
    }

    /**
     * @param message Prompt message the user will see in his terminal.
     * @return the number of the hole he chose (1-X [See Board#HOLES_PER_ROW for X value])
     * @see ConsoleInterface#getPlayerAction(int)
     * In case the player is human, this method is called.
     * This UI's implementation of getting a player's input. In this case, we simply prompt the (human) player with a
     * print, then we read its input from a terminal/DOS box/...
     * @see Board#HOLES_PER_ROW
     */
    private int getPlayerAction(String message) {
        int hole;
        String input = "";
        System.out.print(message);
        Scanner scanner = new Scanner(System.in);
        try {
            input = scanner.nextLine();
            hole = Integer.parseInt(input);
        } catch (NumberFormatException e) {
            if ("q".equalsIgnoreCase(input)) {
                System.out.println(UIMessages.BYE_BYE);
                System.exit(-1);
            }
            return getPlayerAction(UIMessages.VALID_MOVES_RANGE_ERROR);
        }

        if (hole < 1 || hole > Board.HOLES_PER_ROW) {
            return getPlayerAction(UIMessages.VALID_MOVES_RANGE_ERROR);
        }

        return hole - 1;
    }

    @Override
    protected void handlePlayerScored(AwelePlayerScored playerScored) {
        System.out.println(UIMessages.getPlayerScoredMessage(playerScored));
    }

    /**
     * Method called when the BoardController detects that the game is over.
     * In this case, we print the score and the winner in the terminal.
     *
     * @param board Final state of the board.
     * @see AweleOutputMessageHandler
     * @see AweleGameOver
     */
    @Override
    protected void handleGameOver(Board board) {
        this.display(board);
        int[] score = board.scoreHoles;
        int player1Score = score[0];
        int player2Score = score[1];
        String message;
        if (player1Score > player2Score) {
            message = UIMessages.winMessage(0, score);
        } else if (player2Score > player1Score) {
            message = UIMessages.winMessage(1, score);
        } else {
            message = UIMessages.drawMessage(player1Score);
        }

        System.out.println(message);
        System.out.println("What would you like to do? Quit/Restart [ q | r ]: ");
        Scanner scan = new Scanner(System.in);
        String input = scan.nextLine().trim();
        switch (input) {
            case "q":
            case "Q":
                System.out.println(UIMessages.BYE_BYE);
                System.exit(0);
                return;
            case "R":
            case "r":
                Awele.inputMessageStream.publish(AweleInputMessage.requestRestart());
                break;
            default:
                break;
        }
    }

    /**
     * Method called if a player chose a move that doesn't abide by the game rules.
     * We prompt the user to choose a new move if the player is human, else we generate a new move until a valid move is
     * generated.
     *
     * @param error AweleError containing information about why the move isn't valid. Used to print a relevant error
     *              message to the player.
     * @see AweleOutputMessageHandler
     * @see AweleError
     * @see AweleReadyForNewMove
     * @see ConsoleInterface#getPlayerAction(int)
     */
    @Override
    protected void handleError(AweleError error) {
        if (!Awele.getInstance().getPlayers()[error.getPlayerIndex()].isHuman()) {
            return;
        }

        System.out.println(UIMessages.getErrorMessage(error));
    }

    /**
     * Method inherited from AweleEventHandler
     * Method called when the BoardController sends a broadcast to confirm that the move a Player chose was valid and
     * has indeed been played.
     *
     * @param index Index of the move played.
     * @see AweleOutputMessageHandler
     * @see Index
     * @see AweleMoveIsValid
     */
    @Override
    protected void handleValidMove(Index index) {
        System.out.println(UIMessages.chosenMoveMessage(index));
    }

    @Override
    protected void handleRestart() {
        System.out.println("=====================================");
        System.out.println("NEW GAME");
        System.out.println("=====================================");
    }
}
