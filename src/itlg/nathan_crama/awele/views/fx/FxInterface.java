package itlg.nathan_crama.awele.views.fx;

import itlg.nathan_crama.awele.Awele;
import itlg.nathan_crama.awele.message_stream.messages.input.AweleInputMessage;
import itlg.nathan_crama.awele.message_stream.messages.output.AweleError;
import itlg.nathan_crama.awele.message_stream.messages.output.AwelePlayerScored;
import itlg.nathan_crama.awele.models.Board;
import itlg.nathan_crama.awele.models.Index;
import itlg.nathan_crama.awele.views.UIMessages;
import itlg.nathan_crama.awele.views.UserInterface;
import javafx.application.Application;

public class FxInterface extends UserInterface {

    private FxApplication fxApplication;

    public static FxInterface sharedRef;
    private final GameData gameData;
    private boolean isExpectingMove;

    public FxInterface() {
        sharedRef = this;
        this.gameData = new GameData();
        launchApp();
    }

    /**
     * This method is a workaround:
     * As our UI classes extend {@link UserInterface}, we can't extend JavaFX's {@link Application} with a simple
     * {@link Application#launch(String...)}. Moreover, the {@link Application#launch(String...)} method doesn't actuallyh return until
     * the GUI window is closed, which will hang our main thread.
     * To avoid these problems, we use the {@link Application#launch(Class, String...)} method on a new thread.
     */
    private void launchApp() {
        // The app has to be launched from a separate thread or the launch method will only end when the window is
        // closed.
        new Thread(() -> Application.launch(FxApplication.class, "")).start();

        // Since the app is launched from a background thread, we have to make sure it is done launching before
        // interacting with it.
        while (this.fxApplication == null) {
            _wait();
        }

        while (this.fxApplication.getGameView() == null) {
            _wait();
        }
    }

    void setFxApplication(FxApplication fxApplication) {
        this.fxApplication = fxApplication;
    }

    public void sendMove(Index index) {
        isExpectingMove = false;
        Awele.inputMessageStream.publish(AweleInputMessage.newMoveGiven(index));
    }

    @Override
    public void display(Board board) {
        if (fxApplication.getGameView().getGameData() == null) {
            fxApplication.getGameView().setGameData(gameData);
        }
        gameData.setBoardBuffer(board);
    }

    @Override
    protected void handleError(AweleError error) {
        this.gameData.setGameMessages(UIMessages.getErrorMessage(error));
    }

    @Override
    protected void handlePlayerScored(AwelePlayerScored playerScored) {
        this.gameData.setPlayerScoredMessage(UIMessages.getPlayerScoredMessage(playerScored));
    }

    @Override
    protected void handleGameOver(Board board) {
        this.isExpectingMove = false;
        this.gameData.setGameMessages(UIMessages.GAME_OVER_MESSAGE);
        this.display(board);
    }

    public boolean isExpectingMove() {
        return isExpectingMove;
    }

    @Override
    public void handleReadyForNewMove(int row) {
        isExpectingMove = true;
    }

    @Override
    protected void handleRestart() {
        this.gameData.setGameMessages("");
        this.gameData.setPlayerScoredMessage("");
        this.gameData.setLastMovePlayed(null);
    }

    @Override
    protected void handleValidMove(Index index) {
        this.gameData.setGameMessages("");
        this.gameData.setLastMovePlayed(index);
        this.gameData.setTurn(2 - index.getRow());
    }

    private void _wait() {
        try {
            Thread.sleep(1);
        } catch (InterruptedException ignored) {

        }
    }
}
