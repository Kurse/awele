package itlg.nathan_crama.awele.views.fx;

import itlg.nathan_crama.awele.models.Board;
import itlg.nathan_crama.awele.models.Index;
import itlg.nathan_crama.awele.views.UIMessages;
import itlg.nathan_crama.awele.views.fx.components.FxBoardBuffer;
import javafx.beans.property.*;

/**
 * Bean containing JavaFX properties representing our game state.
 * This class only exists to allow data binding between our JavaFX GUI and our game state.
 */
public class GameData {
    private final FxBoardBuffer boardBuffer;
    private final StringProperty lastMovePlayed;
    private final IntegerProperty playerTurn;
    private final StringProperty gameMessages;
    private final StringProperty playerScoredMessage;

    public GameData() {
        this.boardBuffer = new FxBoardBuffer();
        this.lastMovePlayed = new SimpleStringProperty();
        this.playerTurn = new SimpleIntegerProperty();
        this.gameMessages = new SimpleStringProperty();
        this.playerScoredMessage = new SimpleStringProperty();
    }

    public void setBoardBuffer(Board board) {
        this.boardBuffer.setBoard(board);
    }

    public void setLastMovePlayed(Index index) {
        this.lastMovePlayed.set(UIMessages.chosenMoveMessage(index));
    }

    public void setTurn(int turn) {
        this.playerTurn.set(turn);
    }

    public void setGameMessages(String s) {
        this.gameMessages.set(s);
    }

    public void setPlayerScoredMessage(String message) {
        playerScoredMessage.set(message);
    }

    public FxBoardBuffer getBoardBuffer() {
        return boardBuffer;
    }

    public String getLastMovePlayed() {
        return lastMovePlayed.get();
    }

    public StringProperty lastMovePlayedProperty() {
        return lastMovePlayed;
    }

    public int getPlayerTurn() {
        return playerTurn.get();
    }

    public IntegerProperty playerTurnProperty() {
        return playerTurn;
    }

    public String getGameMessages() {
        return gameMessages.get();
    }

    public StringProperty gameMessagesProperty() {
        return gameMessages;
    }

    public String getPlayerScoredMessage() {
        return playerScoredMessage.get();
    }

    public StringProperty playerScoredMessageProperty() {
        return playerScoredMessage;
    }
}
