package itlg.nathan_crama.awele.views.fx;

import itlg.nathan_crama.awele.views.fx.components.FxGameView;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class FxApplication extends Application {

    private FxGameView gameView;

    public FxApplication() {
        super();
        FxInterface.sharedRef.setFxApplication(this);
    }

    @Override
    public void start(Stage window)  {
        this.gameView = new FxGameView(window);

        Scene contentContainer = new Scene(gameView, 500, 270);
        window.setScene(contentContainer);

        window.setTitle("Awele");
        window.show();
    }

    public FxGameView getGameView() {
        return gameView;
    }
}
