package itlg.nathan_crama.awele.views.fx.components;

import itlg.nathan_crama.awele.models.Index;
import itlg.nathan_crama.awele.views.fx.FxInterface;
import javafx.beans.property.IntegerProperty;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Button;

public class Hole extends Button implements EventHandler<ActionEvent> {

    private final Index index;

    public Hole(IntegerProperty title, Index index) {
        setOnAction(this);
        this.index = index;
        int size = 70;
        this.setPrefSize(size, size);
        this.textProperty().bind(title.asString());
    }

    @Override
    public void handle(ActionEvent event) {
        if (FxInterface.sharedRef.isExpectingMove()) {
            FxInterface.sharedRef.sendMove(this.index);
        }
    }

    public Index getIndex() {
        return index;
    }
}
