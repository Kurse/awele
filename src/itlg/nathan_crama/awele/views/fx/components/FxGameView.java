package itlg.nathan_crama.awele.views.fx.components;

import itlg.nathan_crama.awele.Awele;
import itlg.nathan_crama.awele.message_stream.messages.input.AweleInputMessage;
import itlg.nathan_crama.awele.models.Index;
import itlg.nathan_crama.awele.scores.models.Score;
import itlg.nathan_crama.awele.views.fx.GameData;
import javafx.application.Platform;
import javafx.beans.property.IntegerProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.*;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.util.List;

/**
 * Main pane for our JavaFX GUI. Contains the Board view, score holes, UI messages, etc.
 */
public class FxGameView extends BorderPane {

    private final Stage parentStage;
    private GameData gameData;
    private final int padding = 10;

    /**
     * Instantiates a new Fx game view.
     *
     * @param parentStage the parent stage
     */
    public FxGameView(Stage parentStage){
        this.parentStage = parentStage;
    }

    /**
     * Gets game data.
     *
     * @return the game data
     */
    public GameData getGameData() {
        return gameData;
    }

    /**
     * Sets game data.
     *
     * @param board the board
     */
    public void setGameData(GameData board) {
        boolean isFirstInit = this.gameData == null;
        this.gameData = board;

        if (isFirstInit) {
            // It is necessary to run the ui instantiation this way to avoid threading problems.
            Platform.runLater(this::doFirstInit);
        }
    }

    /**
     * Builds the views responsible for displaying the window contents.
     */
    private void doFirstInit() {
        MenuBar menuBar = buildMenuBar();
        this.setTop(menuBar);

        VBox gameView = buildGameView();
        this.setCenter(gameView);
    }


    /**
     * Method used to isolate the creation of the menu bar where you can find options such as restarting the game or
     * displaying the saved scores.
     * @return the menu bar
     */
    private MenuBar buildMenuBar() {
        MenuBar mb = new MenuBar();

        Menu file = new Menu("Menu");
        MenuItem restart = new MenuItem("Recommencer");
        restart.setOnAction(this::onRestartClicked);
        MenuItem score = new MenuItem("Scores");
        score.setOnAction(this::onScoresClicked);
        MenuItem exit = new MenuItem("Quitter");
        exit.setOnAction(this::onExitClicked);

        file.getItems().addAll(restart, score, exit);
        mb.getMenus().addAll(file);

        return mb;
    }

    /**
     * Handles the 'exit' menu item clicks
     * Leaves the game and closes the program
     * @param actionEvent
     */
    private void onExitClicked(ActionEvent actionEvent) {
        System.exit(0);
    }

    /**
     * Handles the 'restart' menu item clicks
     * Restarts the game
     * @param actionEvent
     */
    private void onRestartClicked(ActionEvent actionEvent) {
        Awele.inputMessageStream.publish(AweleInputMessage.requestRestart());
    }

    /**
     * Builds the saved scores grid.
     * @return the built score view.
     */
    private Pane buildScoreView()  {
        List<Score> scores = Awele.getInstance().scoreStorage.fetch();
        ObservableList<Score> s = FXCollections.observableArrayList(scores);

        TableView<Score> tableView = new TableView<>();
        tableView.setEditable(true);
        int colWidth = 200;

        TableColumn<Score, String> nameColumn = new TableColumn<>("Nom joueur 1");
        nameColumn.setCellValueFactory(new PropertyValueFactory<>("playerName"));
        nameColumn.setMinWidth(colWidth);

        TableColumn<Score, Integer> score1Column = new TableColumn<>("Score joueur 1");
        score1Column.setCellValueFactory(new PropertyValueFactory<>("result1"));
        score1Column.setMinWidth(colWidth);

        TableColumn<Score, Integer> score2Column = new TableColumn<>("Score joueur 2");
        score2Column.setCellValueFactory(new PropertyValueFactory<>("result2"));
        score2Column.setMinWidth(colWidth);

        TableColumn<Score, String> dateColumn = new TableColumn<>("Date");
        dateColumn.setCellValueFactory(new PropertyValueFactory<>("start"));
        dateColumn.setMinWidth(colWidth);

        TableColumn<Score, Integer> durationColumn = new TableColumn<>("Durée");
        durationColumn.setCellValueFactory(new PropertyValueFactory<>("duration"));
        durationColumn.setMinWidth(colWidth);

        tableView.getColumns().addAll(nameColumn,
                                      score1Column,
                                      score2Column,
                                      dateColumn,
                                      durationColumn);

        tableView.setItems(s);

        VBox vBox = new VBox();
        vBox.getChildren().add(tableView);
        return vBox;
    }

    /**
     * Handles scores click event.
     * Displays the score view window.
     * @param actionEvent
     */
    private void onScoresClicked(ActionEvent actionEvent) {
        Pane scoreView = buildScoreView();

        Stage dialog = new Stage();
        dialog.setScene(new Scene(scoreView));

        dialog.initOwner(parentStage);
        dialog.initModality(Modality.APPLICATION_MODAL);
        dialog.showAndWait();
    }

    /**
     * Method used to isolate the creation of the various window contents
     */
    private VBox buildGameView() {
        VBox gameView = new VBox();
        gameView.setSpacing(padding);
        gameView.setPadding(new Insets(padding));

        Node topRow = buildTopRow();
        gameView.getChildren().add(topRow);

        Node holesGrid = buildHolesGrid();
        gameView.getChildren().add(holesGrid);

        Node bottomRow = buildBottomRow();
        gameView.getChildren().add(bottomRow);

        TextArea ta = buildPlayerNameTextArea();
        gameView.getChildren().add(ta);

        return gameView;
    }

    /**
     * Method used to isolate the {@link TextArea} where the player can write his/her name.
     */
    private TextArea buildPlayerNameTextArea() {
        TextArea textArea = new TextArea();
        String playerName = Awele.getInstance().getPlayers()[0].getName();
        textArea.setText(playerName);
        textArea.textProperty().addListener(this::onNameTextAreaTextChanged);
        return textArea;
    }

    /**
     * Method responsible for handling text changes within the Name TextArea.
     * It changes the name for the {@link itlg.nathan_crama.awele.models.HumanPlayer} player 1;
     * @param observable ignored
     * @param oldValue previous text content of the TextArea
     * @param newValue new text content of the TextArea
     * @see Awele#getPlayers()
     */
    private void onNameTextAreaTextChanged(ObservableValue<? extends String> observable,
                                           String oldValue,
                                           String newValue) {
        Awele.getInstance().getPlayers()[0].setName(newValue);
    }

    /**
     * Method used to create a pseudo-object representing a horizontal row (in this instance, an {@link HBox}):
     * @return created row
     */
    private HBox row() {
        HBox hbox = new HBox();
        hbox.setPadding(new Insets(padding));
        hbox.setSpacing(30);
        return hbox;
    }

    /**
     * Method used to build the bottom part of the game view containing player1's score and the text field used to
     * display game messages.
     * @return the bottom row of the game View
     */
    private Node buildBottomRow() {
        Pane bottomRow = row();

        Text player1Score = new Text();
        player1Score.textProperty().bind(gameData.getBoardBuffer().scoreHoles[0].asString());

        Text messages = new Text();
        messages.textProperty().bind(gameData.gameMessagesProperty());

        bottomRow.getChildren().addAll(player1Score, messages);
        return bottomRow;
    }

    /**
     * Method used to build the bottom part of the game view containing player2's score, the text field used to display
     * which move was played last and the text field used to display a message when points are scored.
     * @return the top row of the game View
     */
    private Node buildTopRow() {
        HBox topRow = row();

        Text player2ScoreHole = new Text();
        player2ScoreHole.textProperty().bind(gameData.getBoardBuffer().scoreHoles[1].asString());

        Text lastPlayedMoveText = new Text();
        lastPlayedMoveText.textProperty().bind(gameData.lastMovePlayedProperty());

        Text playerScoredText = new Text();
        playerScoredText.textProperty().bind(gameData.playerScoredMessageProperty());

        topRow.getChildren().addAll(player2ScoreHole, lastPlayedMoveText, playerScoredText);
        return topRow;
    }

    /**
     * Builds the grid used to hold the buttons representing the board holes.
     * @return the grid used to hold the buttons
     */
    private GridPane buildHolesGrid() {
        FxBoardBuffer boardBuffer = this.gameData.getBoardBuffer();

        GridPane gp = new GridPane();
        gp.setAlignment(Pos.CENTER);
        gp.setHgap(padding);
        gp.setVgap(padding);
        // Rangee joueur adverse
        int column;
        int row = 0;
        for (column = FxBoardBuffer.HOLES_PER_ROW - 1; column >= 0; --column) {
            IntegerProperty seeds = boardBuffer.holes[1][column];
            Index index = new Index(1, column);
            Hole button = new Hole(seeds, index);
            gp.add(button, (FxBoardBuffer.HOLES_PER_ROW - 1 - column), row);
        }

        row++;
        for (column = 0; column < FxBoardBuffer.HOLES_PER_ROW; ++column) {
            IntegerProperty seeds = boardBuffer.holes[0][column];
            Index index = new Index(0, column);
            Hole button = new Hole(seeds, index);
            gp.add(button, column, row);
        }

        return gp;
    }
}
