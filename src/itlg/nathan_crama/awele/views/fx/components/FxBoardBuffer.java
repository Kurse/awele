package itlg.nathan_crama.awele.views.fx.components;

import itlg.nathan_crama.awele.models.Board;
import javafx.application.Platform;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;

/**
 * Intermediate class to interface between Awele's {@link Board} class and JavaFX properties.
 */
public class FxBoardBuffer {

    public static int ROWS;
    public static int HOLES_PER_ROW;

    static {
        ROWS = Board.ROWS;
        HOLES_PER_ROW = Board.HOLES_PER_ROW;
    }

    IntegerProperty[][] holes;
    IntegerProperty[] scoreHoles;

    public FxBoardBuffer() {
        this.holes = new IntegerProperty[ROWS][HOLES_PER_ROW];
        this.scoreHoles = new SimpleIntegerProperty[ROWS];

        for (int i=0; i<ROWS; i+=1)
        {
            this.scoreHoles[i] = new SimpleIntegerProperty(0);
            for (int j = 0; j < HOLES_PER_ROW; j+=1) {
                this.holes[i][j] = new SimpleIntegerProperty(0);
            }
        }
    }

    /**
     * Instantiates a new Fx board buffer.
     *
     * @param board the board to buffer
     */
    public FxBoardBuffer(Board board) {
        this();
        this.setBoard(board);
    }

    public void setBoard(Board b) {
        Platform.runLater(() -> _setBoard(b));
    }

    private void _setBoard(Board b) {
        Board board = (Board) b.clone();
        for (int i=0; i<ROWS; i+=1)
        {
            this.scoreHoles[i].set(board.scoreHoles[i]);
            for (int j = 0; j < HOLES_PER_ROW; j+=1) {
                this.holes[i][j].set(board.holes[i][j]);
            }
        }
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder()
                .append("| ")
                .append(String.format("%2d", scoreHoles[1].get()))
                .append(" ||\n");

        // Rangee joueur adverse
        for (int i=HOLES_PER_ROW-1; i >= 0; i--) {
            sb.append("| ")
              .append(String.format("%2d", holes[1][i].get()))
              .append(" |");
        }
        sb.append("\n");

        for (int j = 0; j < HOLES_PER_ROW; j++) {
            sb.append("| ")
              .append(String.format("%2d", holes[0][j].get()))
              .append(" |");

        }

        sb.append("\n")
          .append("| ")
          .append(String.format("%2d", scoreHoles[0].get()))
          .append(" ||");
        return sb.toString();
    }
}
