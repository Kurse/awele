package itlg.nathan_crama.awele.views;

import itlg.nathan_crama.awele.Awele;
import itlg.nathan_crama.awele.message_stream.impl.output.AweleOutputMessageHandler;
import itlg.nathan_crama.awele.message_stream.impl.output.AweleOutputMessageStream;
import itlg.nathan_crama.awele.message_stream.messages.input.AweleInputMessage;
import itlg.nathan_crama.awele.message_stream.messages.output.AweleBoardChange;
import itlg.nathan_crama.awele.message_stream.messages.output.AweleError;
import itlg.nathan_crama.awele.message_stream.messages.output.AweleOutputMessage;
import itlg.nathan_crama.awele.models.Board;
import itlg.nathan_crama.awele.models.Index;
import itlg.nathan_crama.awele.models.Player;

/**
 * This class is meant to be a base class for any form of User Interface for our game.
 * It was designed to handle all kinds of UI from TUI to remote server-hosted games.
 * It doesn't take a reference to a game or a Board but instead listens for messages from
 * an AweleEventBus.
 *
 * @see AweleOutputMessageStream
 * @see itlg.nathan_crama.awele.Awele#outputMessageStream
 * @see AweleOutputMessageHandler
 */
public abstract class UserInterface extends AweleOutputMessageHandler {

    /**
     * This constructor subscribes to our main EventBus to listen for new events.
     */
    public UserInterface() {
        Awele.outputMessageStream.subscribe(this);
    }

    /**
     * Method to override in order to display the Board through whatever type of UI is implemented
     *
     * @param board The board to display
     */
    public abstract void display(Board board);

    /**
     * This method is a trick to make sure the control flow will be respected by publishing an {@link
     * AweleReadyForNewMove} event after the UI-specific handling if a new UI is made.
     *
     * @param event
     */
    @Override
    protected void handleInvalidMove(AweleError event) {
        handleError(event);
        Awele.outputMessageStream.publish(AweleOutputMessage.readyForNewMove(event.isHuman(), event.getPlayerIndex()));
    }

    /**
     * Error handling method.
     *
     * @param error {@link AweleError} containing all relevant information for error handling.
     */
    protected abstract void handleError(AweleError error);

    /**
     * Method called whenever the BoardController (or any other class) broadcasts a Board change event.
     *
     * @param board The status of the board after the change.
     * @see AweleBoardChange
     * @see itlg.nathan_crama.awele.controllers.BoardController
     */
    @Override
    public void handleBoardChange(Board board) {
        this.display(board);
    }

    /**
     * Method called when the BoardController is done executing a player's move, and i signalling that it is ready
     * for a new move to be played.
     *
     * @param isHuman Flag determining if the next player to play is a human or not.
     * @param row     The row or player index (0 for player 1, 1 for player 2) to know which row to start from.
     * @see UserInterface#handleReadyForNewMove(int)
     */
    @Override
    public void handleReadyForNewMove(boolean isHuman, int row) {
        if (!isHuman) {
            Index index = Player.generateMove(row);
            Awele.inputMessageStream.publish(AweleInputMessage.newMoveGiven(index));
        } else {
            handleReadyForNewMove(row);
        }
    }

    protected abstract void handleReadyForNewMove(int row);
}
