package itlg.nathan_crama.awele.views;

import itlg.nathan_crama.awele.Awele;
import itlg.nathan_crama.awele.message_stream.messages.output.AweleError;
import itlg.nathan_crama.awele.message_stream.messages.output.AwelePlayerScored;
import itlg.nathan_crama.awele.models.Board;
import itlg.nathan_crama.awele.models.Index;

/**
 * Static class used to store the different Strings used as messages in the User Interface. It should make it easier
 * in the future if we want to translate the game or use the same Strings in another user Interface implementation
 *
 * @see UserInterface
 */
public class UIMessages {
    private UIMessages() {
    }

    static final String VALID_MOVES_RANGE_ERROR = "Votre case doit etre entre 1 et 6: ";
    private static final String UNKNOWN_ERROR_ERROR = "Une erreur inconnue est arrivée. Fin du jeu.";
    static final String PICK_A_HOLE = "Choisissez une case [1-" + Board.HOLES_PER_ROW + "]: ";
    public static final String GAME_OVER_MESSAGE = "Partie finie !";

    private static final String PICK_ANOTHER_ONE = " Veuillez en choisir une autre.";
    static final String BYE_BYE = "Bye bye!";

    static String drawMessage(int score) {
        return "Égalité ! " + score + " points chacun !";
    }

    static String winMessage(int winner, int[] score) {
        return getPlayerName(winner) + " a gagné !\n" + "Score final : [ " + score[0] + " | " + score[1] + " ]";
    }

    public static String chosenMoveMessage(Index i) {
        if (i == null) {
            return "";
        }

        return getPlayerName(i.getRow()) + " a choisi la case " + (i.getHole() + 1) + ".";
    }

    private static String emptyHoleErrorMessage(Index index) {
        return "La case " + (index.getHole() + 1) + " est vide." + PICK_ANOTHER_ONE;
    }

    private static String moveWillStarveErrorMessage(Index index) {
        return "Jouer la case " + (index.getHole() + 1) + " affamera l'adversaire." + PICK_ANOTHER_ONE;
    }

    public static String getErrorMessage(AweleError error) {

        String errorMessage;
        switch (error.getErrorType()) {
            case EMPTY_HOLE:
                errorMessage = UIMessages.emptyHoleErrorMessage(error.getIndex());
                break;
            case OUT_OF_BOUNDS:
                errorMessage = UIMessages.VALID_MOVES_RANGE_ERROR;
                break;
            case MOVE_WILL_STARVE:
                errorMessage = UIMessages.moveWillStarveErrorMessage(error.getIndex());
                break;
            case WRONG_ROW:
                errorMessage = UIMessages.wrongRowErrorMessage(error);
                break;
            default:
                errorMessage = UIMessages.UNKNOWN_ERROR_ERROR;
                System.exit(-1);
                break;
        }

        return errorMessage;
    }

    private static String wrongRowErrorMessage(AweleError error) {
        int playedRow = error.getIndex().getRow();
        int actualRow = error.getPlayerIndex();
        return "Mauvaise rangée: " + "Vous avez joué la rangée " + playedRow + " mais votre rangée est la rangée " +
                actualRow + ". ";
    }

    public static String getPlayerScoredMessage(AwelePlayerScored playerScored) {
        int score = playerScored.getScore();
        String player = getPlayerName(playerScored.getPlayerIndex());

        return player + " a marqué " + score + " points !";
    }

    private static String getPlayerName(int index) {
       return Awele.getInstance().getPlayers()[index].getName();
    }
}
