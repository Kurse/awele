package itlg.nathan_crama.awele;

import itlg.nathan_crama.awele.controllers.BoardController;
import itlg.nathan_crama.awele.message_stream.impl.input.AweleInputMessageStream;
import itlg.nathan_crama.awele.message_stream.impl.output.AweleOutputMessageStream;
import itlg.nathan_crama.awele.message_stream.messages.output.AweleOutputMessage;
import itlg.nathan_crama.awele.models.Board;
import itlg.nathan_crama.awele.models.ComputerPlayer;
import itlg.nathan_crama.awele.models.HumanPlayer;
import itlg.nathan_crama.awele.models.Player;
import itlg.nathan_crama.awele.scores.AweleStorage;
import itlg.nathan_crama.awele.scores.impl.SQLiteStorage;
import itlg.nathan_crama.awele.views.UserInterface;
import itlg.nathan_crama.awele.views.fx.FxInterface;

import java.sql.SQLException;

/**
 * This class is the main game class. Here you can configure which kind of UI you want to use, which kind(s) of
 * player(s) are participating and you can launch the game through {@link Awele#play()}.
 * This class should only be used through its singleton ({@link Awele#instance}) and it holds references to all the
 * parts of the game:
 * {@link Awele#controller}
 * {@link Awele#ui}
 * {@link Awele#players}
 * {@link Awele#outputMessageStream}
 * {@link Awele#inputMessageStream}
 *
 * @see BoardController
 * @see UserInterface
 * @see Player
 * @see AweleOutputMessageStream
 */
public class Awele {
    public static final AweleOutputMessageStream outputMessageStream = new AweleOutputMessageStream();
    public static final AweleInputMessageStream inputMessageStream = new AweleInputMessageStream();

    private static Awele instance;
    public static Awele getInstance() {
        return instance;
    }

    private final BoardController controller;
    private final Player[] players;

    private final UserInterface ui;
    public final AweleStorage scoreStorage;

    private Awele(UserInterface ui, AweleStorage storage) {
        this.players = new Player[]{
                new HumanPlayer("Nathan"),
                new ComputerPlayer()
        };
        this.controller = new BoardController(Board.mainBoard, players);

        this.scoreStorage = storage;
        this.ui = ui;
    }

    public static void main(String[] args) throws SQLException, ClassNotFoundException {
        UserInterface ui = new FxInterface();
        AweleStorage storage = new SQLiteStorage();
        Awele.instance = new Awele(ui, storage);
        Awele.getInstance().play();
    }

    /**
     * Method used to start the game
     */
    private void play() {
        controller.setupBoard();
        this.ui.display(Board.mainBoard);
        outputMessageStream.publish(AweleOutputMessage.gameRestarted());
    }

    public Player[] getPlayers() {
        return players;
    }
}
