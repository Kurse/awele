package itlg.nathan_crama.tools;

import java.io.*;

public abstract class Cloneable implements Serializable {
    private boolean cloned = false;

    public Cloneable clone() {
        Cloneable c = Cloneable.fromByteArray(this.toByteArray());
        c.cloned = true;
        return c;
    }

    public boolean isCloned() {
        return cloned;
    }

    private byte[] toByteArray() {
        byte[] serializedObject = null;
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        ObjectOutputStream out;

        try {
            out = new ObjectOutputStream(outputStream);
            out.writeObject(this);
            out.flush();
            serializedObject = outputStream.toByteArray();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                outputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return serializedObject;
    }

    private static Cloneable fromByteArray(byte[] bytes) {
        Cloneable object = null;
        ByteArrayInputStream inputStream = new ByteArrayInputStream(bytes);
        try (ObjectInput input = new ObjectInputStream(inputStream)) {
            Object o = input.readObject();
            object = (Cloneable) o;
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }

        return object;
    }
}
