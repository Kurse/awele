package itlg.nathan_crama.tools;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class FileUtils {

    public static Path getFilePath(String relativePath)  {
        try {
            String filePath = "";
            if (!relativePath.startsWith("/")) {
                Path currentDir = Paths.get("").toAbsolutePath();
                filePath = currentDir.resolve("").toString();
            }

            String[] splits = relativePath.split("/");
            File file;
            Path path = null;

            for (String split : splits) {
                filePath += "/" + split;
                file = new File(filePath);

                path = Paths.get(filePath);
                if (!file.exists()) {
                    if (!split.equals(splits[splits.length - 1])) {
                        Files.createDirectory(path);
                    } else {
                        Files.createFile(path);
                    }
                }
            }
            return path;
        } catch (IOException e) {
            Logger.error(e);
            return Paths.get(System.getProperty ("java.io.tmpdir"));
        }
    }
}
