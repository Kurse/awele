package itlg.nathan_crama.tools.storage;

import java.io.IOException;
import java.util.List;

public interface PersistentStorage<T> {
    void save(T obj) throws IOException;
    List<T> fetch();
}
