package itlg.nathan_crama.tools.message_stream;

public interface MessageHandler<T extends Message> {
    void handle(T event);
}
