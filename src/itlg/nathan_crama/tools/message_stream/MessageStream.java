package itlg.nathan_crama.tools.message_stream;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Middleman for an observer pattern implementation.
 */
public abstract class MessageStream<T extends Message> {

    private final Map<MessageHandler<T>, ExecutorService> handlers;

    public MessageStream() {
        this.handlers = new HashMap<>();
    }

    /**
     * Method used to subscribe to event broadcasts. The class must implement {@link MessageHandler}.
     *
     * @param handler new event handler to add to our set of Handlers.
     * @see MessageHandler
     */
    public void subscribe(MessageHandler<T> handler) {
        this.handlers.put(handler, Executors.newSingleThreadExecutor());
    }

    /**
     * Broadcast system: Every subscribed {@link MessageHandler} is called and is given the broadcast {@link Message}
     *
     * @param event The Event that is being broadcast
     * @see Message
     * @see MessageStream#handlers
     */
    public void publish(T event) {
        for (Map.Entry<MessageHandler<T>, ExecutorService> entry : handlers.entrySet()) {
            MessageHandler<T> handler = entry.getKey();
            ExecutorService executor = entry.getValue();
            executor.execute(() -> handler.handle(event));
        }
    }
}
