package itlg.nathan_crama.tools;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Logger {
    private static final boolean DEBUG_ACTIVE = false;
    private static final Path path = FileUtils.getFilePath("./out/debug.log");
    private static final ExecutorService thread = Executors.newSingleThreadExecutor();

    static {
        try {
            Files.write(path, new ArrayList<String>(), StandardCharsets.UTF_8);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void log(String prefix, Object s, boolean print) {
        String message = String.format("%s\t%s", prefix, s.toString());
        thread.execute(() -> {
            try {
                List<String> score = new ArrayList<>();
                score.add(LocalDateTime.now() + "  \t" + message);
                Files.write(path, score, StandardCharsets.UTF_8, StandardOpenOption.APPEND);
            } catch (IOException e) {
                e.printStackTrace();
            }
        });

        if (DEBUG_ACTIVE && print) {
            System.out.println(message);
        }
    }

    public static void info(Object s, boolean print) {
        log("INFO:", s, print);
    }

    public static void info(Object s) {
        info(s, true);
    }

    public static void error(Exception s) {
        log("ERROR:", s.getMessage(), true);
        log("", s.getStackTrace(), true);
    }

    public static void error(String s) {
        log("ERROR:", s, true);
    }
}
